Demo: https://weather-report-yvtcwvyykz.now.sh/

This app displays a 5 day weather forecast for a selection of locations. It uses API data from OpenWeatherMap and is built with ReactJS.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## How to build / run / test the project

If you clone the project to run locally, you just need to install dependencies via npm then you will be ready to serve it. 

### `npm install`

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## Running Tests

This app uses [Jest](https://facebook.github.io/jest/) as its test runner. 

Jest is a Node-based runner. This means that the tests always run in a Node environment and not in a real browser. This lets us enable fast iteration speed and prevent flakiness.

While Jest provides browser globals such as `window` thanks to [jsdom](https://github.com/tmpvar/jsdom), they are only approximations of the real browser behavior. Jest is intended to be used for unit tests of your logic and your components rather than the DOM quirks.

We recommend that you use a separate tool for browser end-to-end tests if you need them. They are beyond the scope of Create React App.

## Supported Browsers

By default, the generated project uses the latest version of React.

The app is fully responsive for all modern browsers and devices. The UI was created with a mobile-first consideration. 

## Future Plans (with more time)

* Allow dynamic switching of temperature system = celcius, fahrenheit
* Create autosuggest search input of all available locations in the API
* Proper routing with formed URLs for easy linking
* SEO, customise for each location's page for specific searchs 
* Dynamic background image to change with location/weather conditions
* Use a better icon set than the default one provided by API
* Allow a user-toggleable option to show more advanced weather detail (wind, humidity, pressure etc.)
* More detailed tests