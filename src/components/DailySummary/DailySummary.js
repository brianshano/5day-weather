import React, { Component } from 'react';
import './DailySummary.css';
import PropTypes from 'prop-types';

class DailySummary extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cityId: '',
      cityName: '',
      countryName: '',
      fullForecast: [],
      abridgedForecast: []
    };
  }

  componentWillMount() {
    this.getDailyData(this.props);
  }
  componentWillReceiveProps(nextProps) {
    this.getDailyData(nextProps);
  }

  getDailyData(props) {
    if (props.day.length > 0) {
      let forecastTempMain = [];
      let forecastTempMax = [];
      let forecastTempMin = [];

      let midForecast = props.day.filter((segment, i) => {
        forecastTempMain.push(segment.main.temp);
        forecastTempMax.push(segment.main.temp_max);
        forecastTempMin.push(segment.main.temp_min);
        return segment;
      });

      let maxTemp = forecastTempMax.reduce(function(a, b) {
        return Math.max(a, b);
      });

      let minTemp = forecastTempMin.reduce(function(a, b) {
        return Math.min(a, b);
      });

      this.setState({
        midday: midForecast[0],
        midTemp: Math.round(forecastTempMain[0]),
        maxTemp: Math.round(maxTemp),
        minTemp: Math.round(minTemp)
      });
    }
  }
  render() {
    return (
      <div className="daily-summary">
        <h1>{this.props.dayName}</h1>
        <h2>{this.props.date}</h2>
        <div>
          <img
            src={
              'http://openweathermap.org/img/w/' +
              this.state.midday.weather[0].icon +
              '.png'
            }
            alt="weather icon"
          />
        </div>
        <div className="daily-summary__desc daily-summary__desc--heavy">
          {this.state.midday.weather[0].main}
        </div>
        <div className="daily-summary__desc">
          {this.state.midday.weather[0].description}
        </div>
        <div className="temperatures temperatures__title">&#8451;</div>
        <div className="temperatures__body temperatures__body--flex">
          <div className="temperatures__body--small">
            <div className="temp__title">Lo</div>
            <div className="temp__value">{this.state.minTemp}</div>
          </div>
          <div className="temperatures__body--big">
            <div className="temp__title">Midday</div>
            <div className="temp__value temp__main">{this.state.midTemp}</div>
          </div>
          <div className="temperatures__body--small temp">
            <div className="temp__title">Hi</div>
            <div className="temp__value">{this.state.maxTemp}</div>
          </div>
        </div>
      </div>
    );
  }
}

DailySummary.propTypes = {
  dayName: PropTypes.string,
  date: PropTypes.string
};

export default DailySummary;
