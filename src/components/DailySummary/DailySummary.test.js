import React from 'react';
import ReactDOM from 'react-dom';
import DailySummary from './DailySummary';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const index = 1;
  const day = [
    {
      dt: 1518490800,
      main: {
        temp: -7.96,
        temp_min: -7.96,
        temp_max: -7.69,
        pressure: 1019.57,
        sea_level: 1054.09,
        grnd_level: 1019.57,
        humidity: 62,
        temp_kf: -0.27
      },
      weather: [
        { id: 801, main: 'Clouds', description: 'few clouds', icon: '02n' }
      ],
      clouds: { all: 24 },
      wind: { speed: 3.26, deg: 295.002 },
      rain: {},
      snow: {},
      sys: { pod: 'n' },
      dt_txt: '2018-02-13 03:00:00'
    }
  ];
  const dayName = 'Tuesday';
  const date = '13-02';
  ReactDOM.render(
    <DailySummary key={index} day={day} dayName={dayName} date={date} />,
    div
  );
  ReactDOM.unmountComponentAtNode(div);
});

it('If cityId, show the weather', () => {
  const index = 1;
  const day = [
    {
      dt: 1518490800,
      main: {
        temp: -7.96,
        temp_min: -7.96,
        temp_max: -7.69,
        pressure: 1019.57,
        sea_level: 1054.09,
        grnd_level: 1019.57,
        humidity: 62,
        temp_kf: -0.27
      },
      weather: [
        { id: 801, main: 'Clouds', description: 'few clouds', icon: '02n' }
      ],
      clouds: { all: 24 },
      wind: { speed: 3.26, deg: 295.002 },
      rain: {},
      snow: {},
      sys: { pod: 'n' },
      dt_txt: '2018-02-13 03:00:00'
    }
  ];
  const dayName = 'Tuesday';
  const date = '13-02';
  const tree = renderer
    .create(
      <DailySummary key={index} day={day} dayName={dayName} date={date} />
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
