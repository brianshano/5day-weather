import React, { Component } from 'react';
import './LocationSelector.css';
import PropTypes from 'prop-types';

class LocationSelector extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);

    const cities = [
      {
        cityName: 'London',
        country: 'GB',
        cityId: 2643743
      },
      {
        cityName: 'Dublin',
        country: 'IE',
        cityId: 2964574
      },
      {
        cityName: 'Edinburgh',
        country: 'GB',
        cityId: 2650225
      },
      {
        cityName: 'New York',
        country: 'US',
        cityId: 5128638
      },
      {
        cityName: 'Denver',
        country: 'US',
        cityId: 4463523
      },
      {
        cityName: 'Bangalore',
        country: 'IN',
        cityId: 1277333
      }
    ];

    this.state = {
      selectedCityId: null,
      cities: cities
    };
  }

  handleClick(cityId) {
    this.props.getWeather(cityId);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedCityId: nextProps.selectedCityId
    });
  }

  render() {
    return (
      <div className="location-selector">
        {this.state.cities.map((city, index) => {
          return (
            <button
              className={
                'btn ' +
                (parseFloat(city.cityId) ===
                parseFloat(this.state.selectedCityId)
                  ? 'selected'
                  : 'not-selected')
              }
              key={index}
              onClick={() => this.handleClick(parseInt(city.cityId))}
            >
              {city.cityName}
            </button>
          );
        })}
      </div>
    );
  }
}

LocationSelector.propTypes = {
  getWeather: PropTypes.func,
  selectedCityId: PropTypes.number
};

export default LocationSelector;
