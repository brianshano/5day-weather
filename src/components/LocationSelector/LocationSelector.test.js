import React from 'react';
import ReactDOM from 'react-dom';
import LocationSelector from './LocationSelector';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const cityId = 2964574;
  ReactDOM.render(<LocationSelector selectedCityId={cityId} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('test snapshot of component', () => {
  const cityId = 2964574;
  const tree = renderer
    .create(<LocationSelector selectedCityId={cityId} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
