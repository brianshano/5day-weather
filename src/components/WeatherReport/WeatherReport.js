import React, { Component } from 'react';
import './WeatherReport.css';
import DailySummary from '../DailySummary';
import moment from 'moment';
import PropTypes from 'prop-types';

class WeatherReport extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cityId: '',
      abridgedForecast: []
    };
  }

  componentWillReceiveProps(nextProps) {
    const days = [];
    days.push(moment().format('YYYY-MM-DD'));
    days.push(
      moment()
        .add(1, 'days')
        .format('YYYY-MM-DD')
    );
    days.push(
      moment()
        .add(2, 'days')
        .format('YYYY-MM-DD')
    );
    days.push(
      moment()
        .add(3, 'days')
        .format('YYYY-MM-DD')
    );
    days.push(
      moment()
        .add(4, 'days')
        .format('YYYY-MM-DD')
    );
    days.push(
      moment()
        .add(5, 'days')
        .format('YYYY-MM-DD')
    );

    let forecastsGroup = [];

    days.forEach((day, index) => {
      let dailyData = nextProps.fullForecast.filter(segment => {
        return segment.dt_txt.indexOf(day) > -1;
      });

      forecastsGroup[index] = dailyData;
    });
    // need to remove a day's data if it's empty (sometimes first day)
    forecastsGroup.forEach((day, i) => {
      if (day.length < 1) {
        forecastsGroup.splice(i, 1);
      }
    });

    // only need 5 days - sometimes given 6
    if (forecastsGroup.length > 5) {
      forecastsGroup.splice(-1, 1);
    }

    this.setState({
      cityId: nextProps.cityId,
      abridgedForecast: forecastsGroup
    });
  }

  render() {
    return (
      <div className="weather-report">
        {this.state.cityId ? (
          <div className="weather-report__flex">
            {this.state.abridgedForecast.map((day, index) => {
              let dayName = moment(day[0].dt_txt).format('dddd');
              let date = moment(day[0].dt_txt).format('DD-MM');
              return (
                <DailySummary
                  key={index}
                  day={day}
                  dayName={dayName}
                  date={date}
                />
              );
            })}
          </div>
        ) : (
          <div className="app__welcome">
            Welcome, please choose a city {this.state.cityId}
          </div>
        )}
      </div>
    );
  }
}

WeatherReport.propTypes = {
  cityId: PropTypes.number,
  fullForecast: PropTypes.array
};

export default WeatherReport;
