import React from 'react';
import ReactDOM from 'react-dom';
import WeatherReport from './WeatherReport';
import renderer from 'react-test-renderer';

it('renders without crashing', () => {
  const div = document.createElement('div');
  const cityId = 2964574;
  ReactDOM.render(<WeatherReport selectedCityId={cityId} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('If no cityId, show the welcome message', () => {
  const cityId = null;
  const tree = renderer
    .create(<WeatherReport selectedCityId={cityId} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('If cityId, show the weather', () => {
  const cityId = 2964574;
  const tree = renderer
    .create(<WeatherReport selectedCityId={cityId} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});
