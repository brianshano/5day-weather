import React, { Component } from 'react';
import './App.css';
import LocationSelector from '../components/LocationSelector';
import WeatherReport from '../components/WeatherReport';

class App extends Component {
  constructor() {
    super();

    this.state = {
      cityId: null,
      city: '',
      country: '',
      forecast: []
    };
  }

  getWeather = async cityId => {
    if (typeof cityId === 'undefined') {
      return;
    }
    const appId = '3e03ff96d9c432cb5b74415157369d3f';

    const apiCall = await fetch(
      `https://api.openweathermap.org/data/2.5/forecast?id=${cityId}&APPID=${appId}&units=metric`
    );

    const data = await apiCall.json();

    this.setState({
      cityId: cityId,
      city: data.city.name,
      country: data.city.country,
      forecast: data.list
    });
  };

  render() {
    const { cityId, city, country, forecast } = this.state;
    document.title = '5 Day Weather Forecast';
    if (cityId) {
      document.title = city + ' - ' + document.title;
    }

    return (
      <div className="app">
        <header className="app__header">
          <h1 className="app__title">5 Day Weather Forecast</h1>
        </header>
        <div className="app__container forecast">
          <div className="forecast__header">
            <LocationSelector
              getWeather={this.getWeather}
              selectedCityId={cityId}
            />
          </div>
          <div className="forecast__body">
            <WeatherReport
              cityId={cityId}
              cityName={city}
              countryName={country}
              fullForecast={forecast}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
